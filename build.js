process.env.NODE_ENV = 'production';

const wba = require('webpack-bundle-analyzer');
const webpackConfigProd = require('react-scripts/config/webpack.config.prod');

const { BundleAnalyzerPlugin } = wba;
webpackConfigProd.plugins.push(
  new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    reportFilename: 'report.html',
  }),
);

require('react-scripts/scripts/build');
