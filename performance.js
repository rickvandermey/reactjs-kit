const fs = require('fs');

const index = 'build/index.html';
// Create index HTML in dist
fs.readFile(index, 'utf8', (err, data) => {
  /* eslint no-console: ["error", { allow: ["log", "info"] }] */
  console.info('Reading index.html');
  // let writeData = data.replace('.js">', '.js.gz">'); Only support by Chrome
  const writeData = data.replace('<script src', '<script defer src');

  fs.writeFile(index, writeData, (writeErr) => {
    if (writeErr) { return console.log(writeErr); }
    console.info('index.html was saved!');
    return true;
  });
});
