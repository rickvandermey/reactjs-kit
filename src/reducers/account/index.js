import {
  ACTION_LOAD_ACCOUNT,
  ACTION_RESET_ACCOUNT,
} from './account.constants';

const initialState = {
  hasAccount: false,
  account: {},
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case ACTION_LOAD_ACCOUNT: {
      return Object.assign({}, state, {
        hasAccount: true,
        account: payload,
      });
    }

    case ACTION_RESET_ACCOUNT: {
      return Object.assign({}, initialState);
    }

    default:
      return state;
  }
}
