import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

// locals
import account from './account';

export default history => combineReducers({
  account,
  router: connectRouter(history),
});
