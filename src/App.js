import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ConnectedRouter } from 'connected-react-router';

import renderRoutes from './routes/routes';
import { history, store, persistor } from './store/configureStore';

const App = function () {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ConnectedRouter history={history}>
          { renderRoutes() }
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  );
};

export default App;
