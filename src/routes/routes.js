import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

// Add GoogleAnalytics tracking
import withTracker from './withTracker';

// Views
import Container from '../app/';
import Home from '../views/home';

// ErrorHandling
import Error404 from '../views/error/404';

export default () => (
  <Router>
    <Container>
      <Switch>
        <Route exact path="/" component={withTracker(Home)} />
        <Route component={withTracker(Error404)} />
      </Switch>
    </Container>
  </Router>
);
