import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <section className="page">
        <h1>Welcome!</h1>
        <p>React 16 Application with Redux Store</p>
      </section>
    );
  }
}

const mapStateToProps = (state, ownProps) => Object.assign({}, state, ownProps);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, {
  }), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
