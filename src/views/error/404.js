import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class Error404 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <section className="page">
        <h1>Oops, something went wrong!</h1>
        <p>
          Go back and please try again.
          In case you keep coming back to this page,
          please try again later or contact the hotel directly.
        </p>
      </section>
    );
  }
}

const mapStateToProps = (state, ownProps) => Object.assign({}, state, ownProps);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, {
  }), dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Error404);
