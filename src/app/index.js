import React from 'react';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';

// Used to pass along the route (children) with all necessary compnents
class Container extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { children } = this.props;
    return (
      <React.Fragment>
        { children }
        <ToastContainer />
      </React.Fragment>
    );
  }
}

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Container;
