const fs = require('fs');

const filePath = 'server.pem';
let fileContent = '';

fs.readFile('keys/server.key', 'UTF8', (err, data) => {
  fileContent += data;
});

fs.readFile('keys/server.crt', 'UTF8', (err, data) => {
  fileContent += data;

  fs.writeFile(filePath, fileContent, (writeError) => {
    if (writeError) throw writeError;
    console.log(`Created ${filePath} succesfully`);

    fs.rename(filePath, `node_modules/webpack-dev-server/ssl/${filePath}`, (renameError) => {
      if (renameError) throw renameError;
      console.log('Moved to node_modules/webpack-dev-server/ssl!');
    });
  });
});
