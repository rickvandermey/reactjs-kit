# REACT Boilerplate (react-router / redux / redux-persist / google analytics)

This boilerplate was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). For Further reference check out that repo. This is a boilerplate that will work out of the box with some simple commands.

This boilerplate is an quick and easy setup with Redux and React Router. As an extra there is an easy Google Analytics integration in `./src/routes/withTracker`, Here you can add multiple trackers

## Includes
* [react](https://github.com/facebook/react): ^16.5.1
* [connected-react-router](https://github.com/supasate/connected-react-router): ^4.4.1
* [react-router-dom](https://github.com/ReactTraining/react-router/tree/master/packages/react-router-dom): ^4.3.1
* [redux](https://github.com/reduxjs/redux): ^4.0.0
* [redux-persist](https://github.com/rt2zz/redux-persist): ^5.10.0
* [react-ga](https://github.com/react-ga/react-ga): ^2.5.3
* [react-toastify](https://github.com/fkhadra/react-toastify): ^4.3.1

## Getting started
Install all necessary dependencies with NPM / Yarn.
```
  npm i
```

After all node modules are installed you can run the application.
```
  npm start
```

You're application will be availabile at `localhost:3000`

## Testing
Simple setup with jest

```npm run test```

## Building
simple setup with basics, which can be extended with the extras you like

```npm run build```
